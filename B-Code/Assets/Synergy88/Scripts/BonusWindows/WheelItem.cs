﻿using UnityEngine;
using System.Collections;

public class WheelItem : MonoBehaviour {

	[SerializeField]
	private int _amount;

	public int Amount { get { return _amount; } }

}
