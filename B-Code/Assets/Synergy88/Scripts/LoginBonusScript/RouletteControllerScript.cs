﻿using UnityEngine;
using System.Collections;

public class RouletteControllerScript : MonoBehaviour {

	[SerializeField] private GameObject rouletteHand;
	[SerializeField] private GameObject claimRewardWindow;
	[SerializeField] private GameObject roulettePin;
	[SerializeField] private GameObject roulette;
	//[SerializeField] private GameObject[] priceValue;

	[SerializeField] private tk2dTextMesh prizeWon;
	[SerializeField] private tk2dSprite spinButton;
 	[SerializeField] private tk2dUITweenItem buttonAnim;
	
	private RouletteHandScript pinScript;
	private bool m_spin = true;	

	private GameState _gamestate;

	void Start(){
		pinScript = roulettePin.GetComponent<RouletteHandScript> ();
	}

	void Update(){
		if (_gamestate == GameState.GAMEOVER)
			roulette.transform.Rotate(0,0,10);
	}

	IEnumerator WheelSpin(){
		yield return new WaitForSeconds (0.3f);
		iTween.RotateBy (gameObject, new Vector3 (0, 0, Random.Range(180.0f,50.0f)), 10);
		Invoke ("Finished",10.5f);
		yield return new WaitForSeconds (1.0f);
		m_spin = false;
	}
	
	void spinTheWheel(){
		if (m_spin) {
			AudioManager.Instance.PlayGlobalAudio(AudioManager.GlobalAudioType.STOP_REELS);
			buttonAnim.enabled = false;
			spinButton.color = new Color(0.5f,0.5f,0.5f);
			StartCoroutine (WheelSpin ());
		}
	}

	void Claim(){
		AudioManager.Instance.PlayGlobalAudio (AudioManager.GlobalAudioType.WHEEL_WIN);
		claimRewardWindow.SetActive (false);
		_gamestate = GameState.GAMEOVER;
		//m_spin = true;
		//buttonAnim.enabled = true;
		//spinButton.color = new Color(1.0f,1.0f,1.0f);
		//GameManager.Instance.LoadScene (_MainMenu);
	}
	
	void Finished()
	{
		claimRewardWindow.SetActive (true);
		AudioManager.Instance.PlayGlobalAudio (AudioManager.GlobalAudioType.DAILY_BONUS);
	}
}
