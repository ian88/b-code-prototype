﻿using UnityEngine;
using System.Collections;

public class RouletteHandScript : MonoBehaviour {

	[SerializeField] private tk2dSprite rewardIcon;
	[SerializeField] private GameObject rouletteHand;

	void OnTriggerEnter(Collider col){
		AudioManager.Instance.PlayGlobalAudio (AudioManager.GlobalAudioType.SPIN_TICK);
		rewardIcon.SetSprite(col.gameObject.name);
		//iTween.RotateBy (rouletteHand, new Vector3 (0, 0, -5), 0);
		iTween.RotateBy (rouletteHand, iTween.Hash("z" ,25 , "time" , 0.01f));
		iTween.RotateTo (rouletteHand, iTween.Hash("z" ,0 , "time" , 0, "delay", 0.01f));
	}
}
